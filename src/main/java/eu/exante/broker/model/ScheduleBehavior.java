package eu.exante.broker.model;

/**
 * Policy for orders placed outside of trading hours.
 */
public enum ScheduleBehavior {
    /**
     * Send to the counterparty regardless of schedule.
     */
    AlwaysPlace,

    /**
     * Create the order in pending state and activate when trading opens.
     */
    Delay,

    /**
     * Reject the order immediately.
     */
    Reject
}
