package eu.exante.broker.model;

public enum Side {
    Buy {
        @Override
        public Side reverse() {
            return Sell;
        }
    },
    Sell {
        @Override
        public Side reverse() {
            return Buy;
        }
    };

    public abstract Side reverse();
}
