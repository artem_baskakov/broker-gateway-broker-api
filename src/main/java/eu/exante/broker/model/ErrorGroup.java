package eu.exante.broker.model;

public enum ErrorGroup {
    Other,
    ExanteGeneral,
    ExanteOrderCheck,
    ExanteRiskCheck,
    Counterparty
}
