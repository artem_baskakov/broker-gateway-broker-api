package eu.exante.broker.model;

public enum OrderSource {
    Other,
    ATP,
    FIX,
    Robots,
    Counter,
    Excel,
    Web,
    Mobile
}
