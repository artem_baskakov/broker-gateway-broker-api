package eu.exante.broker.model;

public enum ErrorCode {

    Other(false, "Unknown"),
    BrokerOffline(false, "Broker offline"),
    OrderMissingAtGateway(false, "Unknown order ID"),
    Maintenance(true, "Exchange not available at this time"),
    TimeOutOfSync(false, "Time out of sync"),
    InvalidInstrument(false, "Invalid instrument"),
    InvalidAccount(false, "Invalid account"),
    UnsupportedOrderType(true, "Unsupported order type"),
    UnsupportedDuration(true, "Unsupported duration"),
    InvalidQuantity(true, "Invalid quantity"),
    InvalidLotSize(true, "Order quantity doesn't respect lot size"),
    InvalidPrice(true, "Invalid price"),
    NoMatchingRoutes(true, "Unable to place order with specified parameters"),
    DataMissing(false, "Data missing"),
    InsufficientPermissions(true, "Operation not allowed"),
    ShortingNotAllowed(true, "Instrument not available for short selling"),
    CloseOnlyOrders(true, "Only orders closing position are allowed"),
    ManualLimitBreach(true, "Limit exceeded"),
    InsufficientFreeMoney(true, "Insufficient margin"),
    TooManyActiveOrders(true, "Too many active orders"),
    QuantityOutsideLimits(true, "Order quantity is out of range"),
    PriceOutsideLimits(true, "Price is out of range"),
    TradingTemporarilyUnavailable(true, "Trading not available at this time"),
    OrderTypeTemporarilyUnavailable(true, "Order type not available at this time"),
    OrderDurationTemporarilyUnavailable(true, "Order duration not available at this time"),
    MarketClosed(true, "Market is closed"),
    UnableToModify(true, "Unable to modify order"),
    UnsupportedModificationAction(true, "Unsupported action"),
    PrerequisiteFailed(true, "Prerequisite failed"),
    DuplicateOrderId(false, "Duplicate order ID"),
    RateLimitExceeded(true, "Rate limit exceeded"),
    InstrumentExpired(true, "The instrument has expired");

    private boolean defaultSafe;
    private String defaultMsg;

    ErrorCode(boolean defaultSafe, String defaultMessage) {
        this.defaultSafe = defaultSafe;
        this.defaultMsg = defaultMessage;
    }

    @Deprecated
    public boolean isDefaultSafe() {
        return defaultSafe;
    }

    @Deprecated
    public String getDefaultMessage() {
        return defaultMsg;
    }

    String defaultMessage() {
        return defaultMsg;
    }

    public String clientMessage() {
        if (defaultSafe) {
            return defaultMsg;
        } else {
            return UNSAFE_CLIENT_REJECT;
        }
    }

    static String UNSAFE_CLIENT_REJECT = "Operation rejected";

}
