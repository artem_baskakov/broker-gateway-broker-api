package eu.exante.broker.proto

import java.util

import scala.reflect.ClassTag

private class EnumMapper[Model <: Enum[Model]: ClassTag, Proto <: Enum[Proto]: ClassTag](items: (Model, Proto)*) {
  import EnumMapper._

  private val modelToProto = enumMap[Model, Proto]
  private val protoToModel = enumMap[Proto, Model]

  {
    for ((model, proto) <- items) {
      modelToProto.put(model, proto)
      protoToModel.put(proto, model)
    }

    for (model <- enumValues[Model]) {
      require(modelToProto.containsKey(model), s"No mapping defined for model constant $model")
    }

    for (proto <- enumValues[Proto]) {
      require(protoToModel.containsKey(proto), s"No mapping defined for model constant $proto")
    }
  }

  def toProto(model: Model) = modelToProto.get(model)

  def toModel(proto: Proto) = protoToModel.get(proto)
}

private object EnumMapper {
  def enumMap[K <: Enum[K]: ClassTag, V] = new util.EnumMap[K, V](implicitly[ClassTag[K]].runtimeClass.asInstanceOf[Class[K]])

  def enumValues[T <: Enum[T]: ClassTag]: Seq[T] =
    implicitly[ClassTag[T]].runtimeClass.asInstanceOf[Class[T]].getEnumConstants

}
