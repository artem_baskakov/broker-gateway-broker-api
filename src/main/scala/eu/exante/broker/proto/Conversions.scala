package eu.exante.broker.proto

import java.lang.{Long => JLong}
import java.net.InetAddress
import java.util.UUID

import eu.exante.broker.model
import eu.exante.commons.protobuf.{Decimal, Conversions => CConversions}
import io.protostuff.ByteString
import org.joda.time.{Instant, ReadableInstant}

object Conversions {
  private val errorGroupMapper = new EnumMapper(
    model.ErrorGroup.Other -> ErrorInfo.Group.GROUP_OTHER,
    model.ErrorGroup.Counterparty -> ErrorInfo.Group.COUNTERPARTY,
    model.ErrorGroup.ExanteGeneral -> ErrorInfo.Group.EXANTE_GENERAL,
    model.ErrorGroup.ExanteOrderCheck -> ErrorInfo.Group.EXANTE_ORDER_CHECK,
    model.ErrorGroup.ExanteRiskCheck -> ErrorInfo.Group.EXANTE_RISK_CHECK)

  private val errorCodeMapper = new EnumMapper(
    model.ErrorCode.Other -> ErrorInfo.Code.CODE_OTHER,
    model.ErrorCode.BrokerOffline -> ErrorInfo.Code.BROKER_OFFLINE,
    model.ErrorCode.OrderMissingAtGateway -> ErrorInfo.Code.ORDER_MISSING_AT_GW,
    model.ErrorCode.Maintenance -> ErrorInfo.Code.MAINTENANCE,
    model.ErrorCode.TimeOutOfSync -> ErrorInfo.Code.TIME_OUT_OF_SYNC,
    model.ErrorCode.InvalidInstrument -> ErrorInfo.Code.INVALID_INSTRUMENT,
    model.ErrorCode.InvalidAccount -> ErrorInfo.Code.INVALID_ACCOUNT,
    model.ErrorCode.UnsupportedOrderType -> ErrorInfo.Code.UNSUPPORTED_ORDER_TYPE,
    model.ErrorCode.UnsupportedDuration -> ErrorInfo.Code.UNSUPPORTED_DURATION,
    model.ErrorCode.InvalidQuantity -> ErrorInfo.Code.INVALID_QUANTITY,
    model.ErrorCode.InvalidLotSize -> ErrorInfo.Code.INVALID_LOT_SIZE,
    model.ErrorCode.InvalidPrice -> ErrorInfo.Code.INVALID_PRICE,
    model.ErrorCode.NoMatchingRoutes -> ErrorInfo.Code.NO_MATCHING_ROUTES,
    model.ErrorCode.DataMissing -> ErrorInfo.Code.DATA_MISSING,
    model.ErrorCode.InsufficientPermissions -> ErrorInfo.Code.INSUFFICIENT_PERMISSIONS,
    model.ErrorCode.ShortingNotAllowed -> ErrorInfo.Code.SHORTING_NOT_ALLOWED,
    model.ErrorCode.CloseOnlyOrders -> ErrorInfo.Code.CLOSE_ONLY_ORDERS,
    model.ErrorCode.ManualLimitBreach -> ErrorInfo.Code.MANUAL_LIMIT_BREACH,
    model.ErrorCode.InsufficientFreeMoney -> ErrorInfo.Code.INSUFFICIENT_FREE_MONEY,
    model.ErrorCode.TooManyActiveOrders -> ErrorInfo.Code.TOO_MANY_ACTIVE_ORDERS,
    model.ErrorCode.QuantityOutsideLimits -> ErrorInfo.Code.QUANTITY_OUTSIDE_LIMITS,
    model.ErrorCode.PriceOutsideLimits -> ErrorInfo.Code.PRICE_OUTSIDE_LIMITS,
    model.ErrorCode.TradingTemporarilyUnavailable -> ErrorInfo.Code.TRADING_TEMPORARILY_UNAVAILABLE,
    model.ErrorCode.OrderTypeTemporarilyUnavailable -> ErrorInfo.Code.ORDER_TYPE_TEMPORARILY_UNAVAILABLE,
    model.ErrorCode.OrderDurationTemporarilyUnavailable -> ErrorInfo.Code.ORDER_DURATION_TEMPORARILY_UNAVAILABLE,
    model.ErrorCode.MarketClosed -> ErrorInfo.Code.MARKET_CLOSED,
    model.ErrorCode.UnableToModify -> ErrorInfo.Code.UNABLE_TO_MODIFY,
    model.ErrorCode.UnsupportedModificationAction -> ErrorInfo.Code.UNSUPPORTED_MODIFICATION_ACTION,
    model.ErrorCode.PrerequisiteFailed -> ErrorInfo.Code.PREREQUISITE_FAILED,
    model.ErrorCode.DuplicateOrderId -> ErrorInfo.Code.DUPLICATE_ORDER_ID,
    model.ErrorCode.RateLimitExceeded -> ErrorInfo.Code.RATE_LIMIT_EXCEEDED,
    model.ErrorCode.InstrumentExpired -> ErrorInfo.Code.INSTRUMENT_EXPIRED)

  private val orderSourceMapper = new EnumMapper(
    model.OrderSource.Other -> OrderSource.Type.OTHER,
    model.OrderSource.ATP -> OrderSource.Type.ATP,
    model.OrderSource.FIX -> OrderSource.Type.FIX,
    model.OrderSource.Robots -> OrderSource.Type.ROBOTS,
    model.OrderSource.Counter -> OrderSource.Type.COUNTER,
    model.OrderSource.Excel -> OrderSource.Type.EXCEL,
    model.OrderSource.Web -> OrderSource.Type.WEB,
    model.OrderSource.Mobile -> OrderSource.Type.MOBILE)

  private val sideMapper = new EnumMapper(
    model.Side.Buy -> Side.BUY,
    model.Side.Sell -> Side.SELL)

  implicit def convertUUID(uuid: UUID): ByteString = CConversions.uuidToProto(uuid)
  implicit def convertUUID(uuid: ByteString): UUID = CConversions.uuidFromProto(uuid)

  implicit def convertOptUUID(uuid: Option[UUID]): ByteString = uuid.map(CConversions.uuidToProto).orNull
  implicit def convertOptUUID(uuid: ByteString): Option[UUID] = Option(uuid).map(CConversions.uuidFromProto)

  implicit def convertDecimal(decimal: BigDecimal): Decimal = CConversions.decimalToProto(decimal.underlying())
  implicit def convertDecimal(decimal: Decimal): BigDecimal = CConversions.decimalFromProto(decimal)

  implicit def convertOptDecimal(decimal: Option[BigDecimal]): Decimal =
    if (decimal.isDefined) decimal.get: Decimal else null
  implicit def convertOptDecimal(decimal: Decimal): Option[BigDecimal] =
    if (decimal eq null) None else Some(decimal: BigDecimal)

  implicit def durationToProto(duration: model.OrderDuration): OrderDuration = duration match {
    case model.OrderDuration.Day =>
      new OrderDuration().setType(OrderDuration.Type.DAY)
    case model.OrderDuration.ImmediateOrCancel =>
      new OrderDuration().setType(OrderDuration.Type.IMMEDIATE_OR_CANCEL)
    case model.OrderDuration.FillOrKill =>
      new OrderDuration().setType(OrderDuration.Type.FILL_OR_KILL)
    case model.OrderDuration.GoodTillCancel =>
      new OrderDuration().setType(OrderDuration.Type.GOOD_TILL_CANCEL)
    case model.OrderDuration.AtTheOpening =>
      new OrderDuration().setType(OrderDuration.Type.AT_THE_OPENING)
    case model.OrderDuration.AtTheClose =>
      new OrderDuration().setType(OrderDuration.Type.AT_THE_CLOSE)
    case model.OrderDuration.GoodTillTime(timestamp) =>
      new OrderDuration().setType(OrderDuration.Type.GOOD_TILL_TIME).setGttTimestamp(timestamp.getMillis)
  }

  implicit def durationFromProto(duration: OrderDuration): model.OrderDuration = duration.getType match {
    case OrderDuration.Type.DAY =>
      model.OrderDuration.Day
    case OrderDuration.Type.IMMEDIATE_OR_CANCEL =>
      model.OrderDuration.ImmediateOrCancel
    case OrderDuration.Type.FILL_OR_KILL =>
      model.OrderDuration.FillOrKill
    case OrderDuration.Type.GOOD_TILL_CANCEL =>
      model.OrderDuration.GoodTillCancel
    case OrderDuration.Type.AT_THE_OPENING =>
      model.OrderDuration.AtTheOpening
    case OrderDuration.Type.AT_THE_CLOSE =>
      model.OrderDuration.AtTheClose
    case OrderDuration.Type.GOOD_TILL_TIME =>
      model.OrderDuration.GoodTillTime(new Instant(duration.getGttTimestamp))
  }

  implicit def orderTypeToProto(orderType: model.OrderType): OrderType = orderType match {
    case model.OrderType.Market =>
      new OrderType().setType(OrderType.Type.MARKET)
    case model.OrderType.Limit(limitPrice) =>
      new OrderType().setType(OrderType.Type.LIMIT).setLimitPrice(limitPrice)
    case model.OrderType.Stop(stopPrice) =>
      new OrderType().setType(OrderType.Type.STOP).setStopPrice(stopPrice)
    case model.OrderType.StopLimit(limitPrice, stopPrice) =>
      new OrderType().setType(OrderType.Type.STOP_LIMIT).setLimitPrice(limitPrice).setStopPrice(stopPrice)
  }

  implicit def orderTypeFromProto(orderType: OrderType): model.OrderType = orderType.getType match {
    case OrderType.Type.MARKET =>
      model.OrderType.Market
    case OrderType.Type.LIMIT =>
      model.OrderType.Limit(orderType.getLimitPrice)
    case OrderType.Type.STOP =>
      model.OrderType.Stop(orderType.getStopPrice)
    case OrderType.Type.STOP_LIMIT =>
      model.OrderType.StopLimit(orderType.getLimitPrice, orderType.getStopPrice)
  }

  implicit def orderTypeKindFromProto(orderType: OrderType.Type): model.OrderType.Kind = orderType match {
    case OrderType.Type.MARKET => model.OrderType.Market
    case OrderType.Type.LIMIT => model.OrderType.Limit
    case OrderType.Type.STOP => model.OrderType.Stop
    case OrderType.Type.STOP_LIMIT => model.OrderType.StopLimit
  }

  implicit def orderTypeKindToProto(orderType: model.OrderType.Kind): OrderType.Type = orderType match {
    case model.OrderType.Market => OrderType.Type.MARKET
    case model.OrderType.Limit => OrderType.Type.LIMIT
    case model.OrderType.Stop => OrderType.Type.STOP
    case model.OrderType.StopLimit => OrderType.Type.STOP_LIMIT
  }

  implicit def orderDurationKindFromProto(duration: OrderDuration.Type): model.OrderDuration.Kind = duration match {
    case OrderDuration.Type.DAY => model.OrderDuration.Day
    case OrderDuration.Type.IMMEDIATE_OR_CANCEL => model.OrderDuration.ImmediateOrCancel
    case OrderDuration.Type.FILL_OR_KILL => model.OrderDuration.FillOrKill
    case OrderDuration.Type.GOOD_TILL_CANCEL => model.OrderDuration.GoodTillCancel
    case OrderDuration.Type.AT_THE_OPENING => model.OrderDuration.AtTheOpening
    case OrderDuration.Type.AT_THE_CLOSE => model.OrderDuration.AtTheClose
    case OrderDuration.Type.GOOD_TILL_TIME => model.OrderDuration.GoodTillTime
  }

  implicit def orderDurationKindToProto(duration: model.OrderDuration.Kind): OrderDuration.Type = duration match {
    case model.OrderDuration.Day => OrderDuration.Type.DAY
    case model.OrderDuration.ImmediateOrCancel => OrderDuration.Type.IMMEDIATE_OR_CANCEL
    case model.OrderDuration.FillOrKill => OrderDuration.Type.FILL_OR_KILL
    case model.OrderDuration.GoodTillCancel => OrderDuration.Type.GOOD_TILL_CANCEL
    case model.OrderDuration.AtTheOpening => OrderDuration.Type.AT_THE_OPENING
    case model.OrderDuration.AtTheClose => OrderDuration.Type.AT_THE_CLOSE
    case model.OrderDuration.GoodTillTime => OrderDuration.Type.GOOD_TILL_TIME
  }

  implicit def brokerInfoToProto(brokerInfo: model.BrokerAccount): BrokerAccount =
    new BrokerAccount().
      setName(brokerInfo.name.orNull).
      setClientId(brokerInfo.clientId.orNull)

  implicit def brokerInfoFromProto(brokerInfo: BrokerAccount): model.BrokerAccount =
    model.BrokerAccount(Option(brokerInfo.getName), Option(brokerInfo.getClientId))

  implicit def sideToProto(side: model.Side): Side = sideMapper.toProto(side)
  implicit def sideFromProto(side: Side): model.Side = sideMapper.toModel(side)

  implicit def orderEmulationFlagsToProto(flags: model.OrderEmulationFlags): OrderEmulationFlags =
    new OrderEmulationFlags()
      .setGoodTillCancel(flags.goodTillCancel)
      .setStop(flags.stop)

  implicit def orderEmulationFlagsFromProto(flags: OrderEmulationFlags): model.OrderEmulationFlags =
    if (flags eq null) {
      model.OrderEmulationFlags.empty
    } else {
      model.OrderEmulationFlags(
        goodTillCancel = flags.getGoodTillCancel,
        stop = flags.getStop)
    }

  implicit def errorToProto(error: model.ErrorInfo): ErrorInfo = {
    new ErrorInfo(error.clientMessage, error.internalMessage).setGroup(errorGroupMapper.toProto(error.group)).setCode(errorCodeMapper.toProto(error.code))
  }

  implicit def errorFromProto(error: ErrorInfo): model.ErrorInfo =
    model.ErrorInfo(
      errorGroupMapper.toModel(error.getGroup),
      errorCodeMapper.toModel(error.getCode),
      error.getClientMessage,
      error.getInternalMessage)

  implicit def statusToProto(status: model.OrderStatus): OrderStatus = status match {
    case model.OrderStatus.Created => new OrderStatus(OrderStatus.Type.CREATED)
    case model.OrderStatus.Accepted => new OrderStatus(OrderStatus.Type.ACCEPTED)
    case model.OrderStatus.Placing => new OrderStatus(OrderStatus.Type.PLACING)
    case model.OrderStatus.Working => new OrderStatus(OrderStatus.Type.WORKING)
    case model.OrderStatus.Filled => new OrderStatus(OrderStatus.Type.FILLED)
    case model.OrderStatus.Cancelled => new OrderStatus(OrderStatus.Type.CANCELLED)
    case model.OrderStatus.Pending => new OrderStatus(OrderStatus.Type.PENDING)
    case model.OrderStatus.Rejected(error) => new OrderStatus(OrderStatus.Type.REJECTED).setRejectReason(error)
  }

  implicit def statusFromProto(status: OrderStatus): model.OrderStatus = status.getType match {
    case OrderStatus.Type.CREATED => model.OrderStatus.Created
    case OrderStatus.Type.ACCEPTED => model.OrderStatus.Accepted
    case OrderStatus.Type.PLACING => model.OrderStatus.Placing
    case OrderStatus.Type.WORKING => model.OrderStatus.Working
    case OrderStatus.Type.FILLED => model.OrderStatus.Filled
    case OrderStatus.Type.CANCELLED => model.OrderStatus.Cancelled
    case OrderStatus.Type.PENDING => model.OrderStatus.Pending
    case OrderStatus.Type.REJECTED => model.OrderStatus.Rejected(status.getRejectReason)
  }

  implicit def sourceToProto(source: model.OrderSource): OrderSource =
    new OrderSource().setType(orderSourceMapper.toProto(source))

  implicit def sourceFromProto(source: OrderSource): model.OrderSource =
    orderSourceMapper.toModel(source.getType)

  implicit def originatorToProto(source: model.Originator): Originator = {
    val result = new Originator()
      .setUsername(source.username)
      .setSource(source.source)
      .setModule(source.module.orNull)
      .setTag(source.tag.orNull)
    source.address match {
      case None =>
      case Some(addr) => result.setAddress(ByteString.copyFrom(addr.getAddress))
    }
    result
  }

  implicit def originatorFromProto(source: Originator): model.Originator = {
    val address = source.getAddress match {
      case null => None
      case addr => Some(InetAddress.getByAddress(addr.toByteArray))
    }
    model.Originator(
      source.getUsername,
      source.getSource,
      address = address,
      module = Option(source.getModule),
      tag = Option(source.getTag))
  }

  implicit def eventTimestampToProto(source: model.EventTimestamp): EventTimestamp = {
    val result = new EventTimestamp(source.localTime.getMillis)
    source.counterpartyTime match {
      case None =>
      case Some(time) => result.setCounterpartyTime(time.getMillis)
    }
    result
  }

  implicit def eventTimestampFromProto(source: EventTimestamp): model.EventTimestamp = {
    val localTime = new Instant(source.getLocalTime.longValue())
    val counterpartyTime = source.getCounterpartyTime match {
      case null => None
      case time => Some(new Instant(time.longValue()))
    }
    model.EventTimestamp(localTime, counterpartyTime)
  }

  implicit def instantToProto(source: ReadableInstant): JLong = source.getMillis

  implicit def instantFromProto(source: JLong): Instant = new Instant(source.longValue())

  implicit def optInstantToProto(source: Option[ReadableInstant]): JLong = source match {
    case None => null
    case Some(instant) => Long.box(instant.getMillis)
  }

  implicit def optInstantFromProto(source: JLong): Option[Instant] =
    if (source eq null) {
      None
    } else {
      Some(new Instant(source.longValue()))
    }

  implicit def scheduleBehaviorToProto(behavior: model.ScheduleBehavior): ScheduleBehavior = behavior match {
    case model.ScheduleBehavior.AlwaysPlace => ScheduleBehavior.ALWAYS_PLACE
    case model.ScheduleBehavior.Delay => ScheduleBehavior.DELAY
    case model.ScheduleBehavior.Reject => ScheduleBehavior.REJECT
  }

  def scheduleBehaviorFromProto(behavior: ScheduleBehavior, canBeDelayed: Boolean): model.ScheduleBehavior = behavior match {
    case ScheduleBehavior.ALWAYS_PLACE => model.ScheduleBehavior.AlwaysPlace
    case ScheduleBehavior.DELAY => model.ScheduleBehavior.Delay
    case ScheduleBehavior.REJECT => model.ScheduleBehavior.Reject
    case ScheduleBehavior.LEGACY if canBeDelayed => model.ScheduleBehavior.Delay
    case ScheduleBehavior.LEGACY => model.ScheduleBehavior.AlwaysPlace
  }
}
