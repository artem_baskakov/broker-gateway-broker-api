package eu.exante.broker.json.spray

import java.net.InetAddress
import java.util.UUID

import com.google.common.net.InetAddresses
import eu.exante.broker.json.spray.CommonFormats._
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{Instant, LocalDate}
import spray.json._

trait CommonFormats {
  abstract class StringBasedFormat[T](parse: String => T, print: T => String) extends JsonFormat[T] {
    def read(json: JsValue) = parse(json.convertTo[String])
    def write(obj: T) = print(obj).toJson
  }

  implicit object UUIDFormat extends StringBasedFormat[UUID](UUID.fromString, _.toString)

  implicit object TimestampFormat extends StringBasedFormat[Instant](dateTimeParser.parseDateTime(_).toInstant, dateTimeFormatter.print)
  implicit object LocalDateFormat extends StringBasedFormat[LocalDate](dateTimeParser.parseLocalDate, dateFormatter.print)

  implicit def enumMapperFormat[T <: Enum[T] : EnumMapper]: JsonFormat[T] = new JsonFormat[T] {
    val mapper = implicitly[EnumMapper[T]]
    override def write(obj: T) = mapper.write(obj).toJson
    override def read(json: JsValue) = mapper.read(json.convertTo[String])
  }

  implicit object InetAddressFormat extends StringBasedFormat[InetAddress](
    InetAddresses.forString,
    _.getHostAddress)

  implicit def optionWriter[T: JsonWriter]: JsonWriter[Option[T]] = new JsonWriter[Option[T]] {
    def write(obj: Option[T]) = obj.map(x => x.toJson).getOrElse(JsNull)
  }

  def singleton[A](value: A) = jsonFormat0(() => value)
}

object CommonFormats extends CommonFormats {
  val dateTimeFormatter = ISODateTimeFormat.dateTime()
  val dateTimeParser = ISODateTimeFormat.dateOptionalTimeParser()
  val dateFormatter = ISODateTimeFormat.date().withZoneUTC()
}
