package eu.exante.broker.json.spray

import eu.exante.broker.json.spray.TypedFormat._
import spray.json._

import scala.collection.immutable.ListMap


case class TypedFormat[T: Manifest] private(typeField: String,
                                            writeMappers: Map[Class[T], WriteMapper[T]],
                                            readMappers: Map[String, JsonReader[T]]) extends RootJsonFormat[T] {
  private def rootType = implicitly[Manifest[T]].runtimeClass.getName

  def register[A <: T](typeName: String, format: JsonFormat[A])(implicit manifest: Manifest[A]): TypedFormat[T] = {
    val cls = implicitly[Manifest[A]].runtimeClass.asInstanceOf[Class[T]]

    val fmt = format.asInstanceOf[JsonFormat[T]]

    require(!writeMappers.contains(cls), s"TypedFormat[$rootType] $cls already registered")
    require(!readMappers.contains(typeName), s"TypedFormat[$rootType]: $typeName already registered")
    copy(
      writeMappers = writeMappers + (cls -> WriteMapper(typeName, fmt)),
      readMappers = readMappers + (typeName -> fmt))
  }

  def read(json: JsValue) = {
    val obj = json.asJsObject
    obj.fields.get(typeField) match {
      case Some(JsString(typeName)) =>
        val reader = readMappers.getOrElse(typeName, deserializationError(s"Unknown $rootType type: $typeName"))
        reader.read(obj)
      case Some(other) =>
        deserializationError(s"Invalid value of $typeField: $other (while deserializing $rootType)")
      case None =>
        deserializationError(s"Unable to find $typeField (while deserializing $rootType)")
    }
  }

  def write(obj: T) = {
    val WriteMapper(typeName, mapper) = writeMappers.getOrElse(obj.getClass.asInstanceOf[Class[T]], serializationError(s"Unknown $rootType: $obj"))
    val value = mapper.write(obj).asJsObject
    JsObject(ListMap(typeField -> typeName.toJson) ++ value.fields)
  }
}

object TypedFormat {
  case class WriteMapper[T](typeName: String, writer: JsonWriter[T])

  def of[T: Manifest](typeField: String) = TypedFormat[T](typeField, Map.empty, Map.empty)
}
