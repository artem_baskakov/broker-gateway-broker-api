package eu.exante.broker.json.spray

import java.util

import scala.collection.JavaConverters._
import scala.reflect.ClassTag

class EnumMapper[T <: Enum[T]: ClassTag] {
  import eu.exante.broker.json.spray.EnumMapper._

  private val toDbMap = new util.EnumMap[T, String](implicitly[ClassTag[T]].runtimeClass.asInstanceOf[Class[T]]).asScala
  private val fromDbMap = new util.HashMap[String, T].asScala

  {
    val enumValues = implicitly[ClassTag[T]].runtimeClass.asInstanceOf[Class[T]].getEnumConstants

    for (value <- enumValues) {
      val dbName = splitCase(value.name()).map(_.toLowerCase).mkString("_")
      require(!fromDbMap.contains(dbName), s"Mapping for $value ($$dbName conflicts with ${fromDbMap.get(dbName)}")

      toDbMap.put(value, dbName)
      fromDbMap.put(dbName, value)
    }
  }

  def write(value: T) = toDbMap(value)
  def read(value: String): T = fromDbMap.getOrElse(value, throw new IllegalArgumentException(s"No mapping for enum constant $value"))
  
  def stringValues: Set[String] = fromDbMap.keySet.toSet
}

object EnumMapper {
  private val CaseBoundary = "(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])"

  private def splitCase(word: String) = word.split(CaseBoundary)
}