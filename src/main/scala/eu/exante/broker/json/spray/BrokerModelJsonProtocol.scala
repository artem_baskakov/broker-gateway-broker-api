package eu.exante.broker.json.spray

import spray.json.DefaultJsonProtocol

trait BrokerModelJsonProtocol
  extends DefaultJsonProtocol
  with CommonFormats
  with BrokerModelFormats
  with BrokerEnumMapperInstances

object BrokerModelJsonProtocol extends BrokerModelJsonProtocol
