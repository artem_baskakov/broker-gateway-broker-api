package eu.exante.broker.json

import _root_.spray.json.{DefaultJsonProtocol, NullOptions}

package object spray extends DefaultJsonProtocol with NullOptions
