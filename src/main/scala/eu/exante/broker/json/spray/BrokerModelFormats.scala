package eu.exante.broker.json.spray

import java.net.InetAddress

import eu.exante.broker.json.spray.BrokerEnumMapperInstances._
import eu.exante.broker.json.spray.CommonFormats._
import eu.exante.broker.model._
import spray.json._

trait BrokerModelFormats {
  implicit val eventTimestampFormat = jsonFormat(EventTimestamp.apply, "local", "counterparty")
  implicit val brokerAccountFormat = jsonFormat2(BrokerAccount)

  implicit object OriginatorFormat extends JsonFormat[Originator] {
    def read(json: JsValue) = {
      val obj = json.asJsObject
      obj.getFields("username", "source") match {
        case Seq(username, source) =>
          Originator(
            username.convertTo[String],
            source.convertTo[OrderSource],
            module = obj.fields.getOrElse("sourceModule", JsNull).convertTo[Option[String]],
            tag = obj.fields.getOrElse("sourceTag", JsNull).convertTo[Option[String]],
            address = obj.fields.getOrElse("sourceAddress", JsNull).convertTo[Option[InetAddress]])
        case _ => deserializationError("Originator expected")
      }
    }

    def write(obj: Originator) = JsObject(
      "username" -> obj.username.toJson,
      "source" -> obj.source.toJson,
      "sourceAddress" -> obj.address.toJson,
      "sourceModule" -> obj.module.toJson,
      "sourceTag" -> obj.tag.toJson)
  }

  implicit val OrderTypeFormat = TypedFormat.of[OrderType]("type")
    .register("market", singleton(OrderType.Market))
    .register("limit", jsonFormat1(OrderType.Limit.apply))
    .register("stop", jsonFormat1(OrderType.Stop.apply))
    .register("stop_limit", jsonFormat2(OrderType.StopLimit.apply))

  implicit val OrderDurationFormat = TypedFormat.of[OrderDuration]("duration")
    .register("day", singleton(OrderDuration.Day))
    .register("fill_or_kill", singleton(OrderDuration.FillOrKill))
    .register("immediate_or_cancel", singleton(OrderDuration.ImmediateOrCancel))
    .register("good_till_cancel", singleton(OrderDuration.GoodTillCancel))
    .register("good_till_time", jsonFormat(OrderDuration.GoodTillTime.apply _, "gttExpiration"))
    .register("at_the_opening", singleton(OrderDuration.AtTheOpening))
    .register("at_the_close", singleton(OrderDuration.AtTheClose))

  implicit object ErrorInfoFormat extends RootJsonFormat[ErrorInfo] {
    override def read(json: JsValue) = json.asJsObject.getFields("group", "code", "clientMessage", "internalMessage") match {
      case Seq(group, code, clientMsg, internalMsg) =>
        ErrorInfo(group.convertTo[ErrorGroup], code.convertTo[ErrorCode], clientMsg.convertTo[String], internalMsg.convertTo[String])
      case _ => deserializationError("Expected ErrorInfo")
    }
    override def write(obj: ErrorInfo) = JsObject(
      "group" -> obj.group.toJson,
      "code" -> obj.code.toJson,
      "clientMessage" -> obj.clientMessage.toJson,
      "internalMessage" -> obj.internalMessage.toJson)
  }

  implicit val OrderStatusFormat = TypedFormat.of[OrderStatus]("status")
    .register("created", singleton(OrderStatus.Created))
    .register("accepted", singleton(OrderStatus.Accepted))
    .register("placing", singleton(OrderStatus.Placing))
    .register("working", singleton(OrderStatus.Working))
    .register("cancelled", singleton(OrderStatus.Cancelled))
    .register("pending", singleton(OrderStatus.Pending))
    .register("filled", singleton(OrderStatus.Filled))
    .register("rejected", jsonFormat1(OrderStatus.Rejected))

}

object BrokerModelFormats extends BrokerModelFormats
