package eu.exante.broker.json.spray

import eu.exante.broker.model._

trait BrokerEnumMapperInstances {
  implicit val orderSourceMapper = new EnumMapper[OrderSource]
  implicit val sideMapper = new EnumMapper[Side]
  implicit val errorCodeMapper = new EnumMapper[ErrorCode]
  implicit val errorGroupMapper = new EnumMapper[ErrorGroup]
  implicit val scheduleBehaviorMapper = new EnumMapper[ScheduleBehavior]
}

object BrokerEnumMapperInstances extends BrokerEnumMapperInstances
