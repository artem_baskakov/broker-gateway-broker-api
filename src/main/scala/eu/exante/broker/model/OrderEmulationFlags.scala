package eu.exante.broker.model

case class OrderEmulationFlags(goodTillCancel: Boolean, stop: Boolean)

object OrderEmulationFlags {

  val empty = OrderEmulationFlags(false, false)
  val all = OrderEmulationFlags(true, true)

}