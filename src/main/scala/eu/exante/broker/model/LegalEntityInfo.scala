package eu.exante.broker.model

case class LegalEntityInfo(user: String, account: String, counterparty: String,
                           counterUser: Option[String], counterAccount: Option[String],
                           markupUser: Option[String], markupAccount: Option[String])
