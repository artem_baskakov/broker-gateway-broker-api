package eu.exante.broker.model

sealed trait OrderStatus {
  def isActive: Boolean
  def isTerminated = !isActive
}

object OrderStatus {
  sealed trait Active extends OrderStatus {
    def isActive = true
  }
  sealed trait Terminated extends OrderStatus {
    def isActive = false
  }
  sealed trait External extends OrderStatus

  case object Created extends OrderStatus with Active
  case object Pending extends OrderStatus with Active
  case object Accepted extends OrderStatus with Active

  case object Placing extends OrderStatus with Active with External
  case object Working extends OrderStatus with Active with External

  case object Filled extends OrderStatus with Terminated with External
  case object Cancelled extends OrderStatus with Terminated with External
  case class Rejected(reason: ErrorInfo) extends OrderStatus with Terminated with External

}
