package eu.exante.broker.model

import org.joda.time.{Instant, ReadableInstant}

case class EventTimestamp(localTime: Instant, counterpartyTime: Option[Instant])
object EventTimestamp {
  def local(time: ReadableInstant) = EventTimestamp(time.toInstant, None)
  def counterparty(localTime: ReadableInstant, counterpartyTime: ReadableInstant) = EventTimestamp(localTime.toInstant, Some(counterpartyTime.toInstant))
}
