package eu.exante.broker.model

case class BrokerAccount(name: Option[String], clientId: Option[String])
