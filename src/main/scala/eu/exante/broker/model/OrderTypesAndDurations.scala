package eu.exante.broker.model

import com.ghcg.commons.json.JSONHelpers._
import com.google.gson.{JsonArray, JsonObject}

object OrderTypesAndDurations {

  private def parseDurations(from: JsonArray): Set[OrderDuration.Kind] = {
    val result = for {
      elem <- from.view
      durationName = elem.getAsString
      if durationName != "DISABLED"
      duration <- OrderDuration.parse(durationName)
    } yield duration
    result.toSet
  }

  def parse(from: JsonObject): Map[OrderType.Kind, Set[OrderDuration.Kind]] = {
    val result = for {
      (jsonType, jsonDurations) <- from.view
      orderType <- OrderType.parse(jsonType)
      orderDurations = parseDurations(jsonDurations.getAsJsonArray)
      if orderDurations.nonEmpty
    } yield orderType -> orderDurations
    result.toMap
  }

}
