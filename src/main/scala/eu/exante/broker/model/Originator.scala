package eu.exante.broker.model

import java.net.InetAddress

import scala.collection.mutable.ArrayBuffer

case class Originator(username: String, source: OrderSource,
                      module: Option[String] = None,
                      tag: Option[String] = None,
                      address: Option[InetAddress] = None) {

  private def sourceInfo = {
    val parts = ArrayBuffer.empty[String]
    parts ++= module
    parts ++= tag

    parts.mkString("/") match {
      case none if none.isEmpty => source.toString
      case items => s"$source ($items)"
    }
  }

  private def addressInfo = address match {
    case None => ""
    case Some(addr) => s" from ${addr.getHostAddress}"
  }

  override def toString = s"Originator($username via $sourceInfo$addressInfo)"

}