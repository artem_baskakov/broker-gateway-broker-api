package eu.exante.broker.model

sealed trait OrderType {
  def kind: OrderType.Kind

  def withPrices(newLimitPrice: Option[BigDecimal], newStopPrice: Option[BigDecimal]): OrderType

  def transformPrices(transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal): OrderType
}

object OrderType {

  def parse(from: String): Option[OrderType.Kind] = PartialFunction.condOpt(from) {
    case "MARKET" => OrderType.Market
    case "LIMIT" => OrderType.Limit
    case "STOP" => OrderType.Stop
    case "STOP_LIMIT" => OrderType.StopLimit
  }

  trait LimitLike {
    def limitPrice: BigDecimal
  }

  trait StopLike {
    def stopPrice: BigDecimal
  }

  sealed trait Kind {
    def matches(orderType: OrderType): Boolean
  }

  case object Market extends OrderType with Kind {
    def kind: this.type = this

    def matches(orderType: OrderType) = orderType == this

    def withPrices(newLimitPrice: Option[BigDecimal], newStopPrice: Option[BigDecimal]) = this

    def transformPrices(transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal) = this
  }

  case class Limit(limitPrice: BigDecimal) extends OrderType with LimitLike {
    def kind = Limit

    def withPrices(newLimitPrice: Option[BigDecimal], newStopPrice: Option[BigDecimal]) = copy(limitPrice = newLimitPrice.getOrElse(limitPrice))

    def transformPrices(transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal) = copy(limitPrice = transformLimitPrice(limitPrice))
  }
  case object Limit extends Kind {
    def matches(orderType: OrderType) = orderType.isInstanceOf[Limit]
  }

  case class Stop(stopPrice: BigDecimal) extends OrderType with StopLike {
    def kind = Stop

    def withPrices(newLimitPrice: Option[BigDecimal], newStopPrice: Option[BigDecimal]) = copy(stopPrice = newStopPrice.getOrElse(stopPrice))

    def transformPrices(transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal) = copy(stopPrice = transformStopPrice(stopPrice))
  }
  case object Stop extends Kind {
    def matches(orderType: OrderType) = orderType.isInstanceOf[Stop]
  }

  case class StopLimit(limitPrice: BigDecimal, stopPrice: BigDecimal) extends OrderType with LimitLike with StopLike {
    def kind = StopLimit

    def withPrices(newLimitPrice: Option[BigDecimal], newStopPrice: Option[BigDecimal]) =
      copy(limitPrice = newLimitPrice.getOrElse(limitPrice), stopPrice = newStopPrice.getOrElse(stopPrice))

    def transformPrices(transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal) =
      copy(limitPrice = transformLimitPrice(limitPrice), stopPrice = transformStopPrice(stopPrice))
  }
  case object StopLimit extends Kind {
    def matches(orderType: OrderType) = orderType.isInstanceOf[StopLimit]
  }

  // TODO deprecate soon
  def transformPrices(orderType: OrderType, transformLimitPrice: BigDecimal => BigDecimal, transformStopPrice: BigDecimal => BigDecimal): OrderType =
    orderType.transformPrices(transformLimitPrice, transformStopPrice)

  def limitPrice(from: OrderType) = from match {
    case l: OrderType.LimitLike => Some(l.limitPrice)
    case _ => None
  }

  def stopPrice(from: OrderType) = from match {
    case s: OrderType.StopLike => Some(s.stopPrice)
    case _ => None
  }
}
