package eu.exante.broker.model

import com.google.gson.JsonObject
import com.ghcg.commons.json.JSONHelpers._
import eu.exante.symboldb.ScheduleParser
import org.joda.time.{Interval, LocalDate}

object TradingInterval {
  private val intervalParser = ScheduleParser.scheduleParser[TradingInterval] { (rawInterval, baseInterval) =>
    val typesAndDurations = rawInterval
      .opt("availableOrderDurationTypes")(_.getAsJsonObject)
      .map(OrderTypesAndDurations.parse)
      .getOrElse(Map.empty)

    val intervalType = rawInterval.getAsString("type")
    TradingInterval(baseInterval.period, baseInterval.sessionDate, intervalType == "OFFLINE",
      intervalType == "MAIN_SESSION", typesAndDurations)
  }

  def parse(json: JsonObject): Seq[TradingInterval] = intervalParser(json)

  sealed trait Allowance
  case object NoneAllowance extends Allowance
  case object OrderTypeAllowance extends Allowance
  case object OrderTypeAndDurationAllowance extends Allowance
}

case class TradingInterval(interval: Interval, sessionDate: LocalDate, offline: Boolean, mainSession: Boolean,
                           availableOrderTypesAndDurations: Map[OrderType.Kind, Set[OrderDuration.Kind]]) {

  def withStartOffset(delay: Long): TradingInterval = copy(interval = interval.withStartMillis(interval.getStartMillis + delay))

  import TradingInterval._

  def allows(orderType: OrderType.Kind, orderDuration: OrderDuration.Kind): Allowance =
    availableOrderTypesAndDurations.get(orderType) match {
      case Some(durations) if durations.contains(orderDuration) => OrderTypeAndDurationAllowance
      case Some(_) => OrderTypeAllowance
      case None => NoneAllowance
    }

}
