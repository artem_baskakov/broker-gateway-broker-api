package eu.exante.broker.model

case class ModificationEmulationFlags(replace: Boolean)

object ModificationEmulationFlags {
  val empty = ModificationEmulationFlags(false)

  val all = ModificationEmulationFlags(true)
}