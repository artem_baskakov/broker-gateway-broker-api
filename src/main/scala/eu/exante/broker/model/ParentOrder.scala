package eu.exante.broker.model

import java.util.UUID

case class ParentOrder(id: UUID, fills: Int)