package eu.exante.broker.model

trait ErrorInfo {
  def group: ErrorGroup
  def code: ErrorCode
  def clientMessage: String
  def internalMessage: String
}

object ErrorInfo {

  /**
   * Remove null character from messages (amro is a known offender)
   */
  private def sanitize(message: String) = message.replace('\u0000', '0')

  private[broker] case class Impl(group: ErrorGroup, code: ErrorCode, clientMessage: String, internalMessage: String) extends ErrorInfo {
    override def toString = s"Error[$group/$code](client message $clientMessage, internal message $internalMessage)"
  }

  def apply(group: ErrorGroup, code: ErrorCode, clientMessage: String, internalMessage: String): ErrorInfo =
    Impl(group, code, sanitize(clientMessage), sanitize(internalMessage))

  def safe(group: ErrorGroup, code: ErrorCode, message: String): ErrorInfo =
    apply(group, code, message, message)

  def unsafe(group: ErrorGroup, code: ErrorCode, message: String): ErrorInfo =
    apply(group, code, ErrorCode.UNSAFE_CLIENT_REJECT, message)

  def default(group: ErrorGroup, code: ErrorCode) =
    apply(group, code, code.clientMessage(), code.defaultMessage())

  def detailed(group: ErrorGroup, code: ErrorCode, internalMessage: String): ErrorInfo =
    apply(group, code, code.clientMessage(), internalMessage)


}
