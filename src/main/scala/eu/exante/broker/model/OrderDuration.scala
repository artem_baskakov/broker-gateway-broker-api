package eu.exante.broker.model

import org.joda.time.{Instant, ReadableInstant}

sealed trait OrderDuration {
  def kind: OrderDuration.Kind
}

object OrderDuration {

  def parse(from: String): Option[OrderDuration.Kind] = PartialFunction.condOpt(from) {
    case "DAY" => OrderDuration.Day
    case "GOOD_TILL_CANCEL" => OrderDuration.GoodTillCancel
    case "GOOD_TILL_TIME" => OrderDuration.GoodTillTime
    case "IMMEDIATE_OR_CANCEL" => OrderDuration.ImmediateOrCancel
    case "FILL_OR_KILL" => OrderDuration.FillOrKill
    case "AT_THE_OPENING" => OrderDuration.AtTheOpening
    case "AT_THE_CLOSE" => OrderDuration.AtTheClose
  }

  sealed trait Kind {
    def matches(duration: OrderDuration): Boolean
  }

  sealed trait Parameterless extends OrderDuration with Kind {
    def kind: this.type = this
    def matches(duration: OrderDuration) = duration == this
  }

  case object Day extends OrderDuration with Parameterless

  case object ImmediateOrCancel extends OrderDuration with Parameterless

  case object FillOrKill extends OrderDuration with Parameterless

  case object GoodTillCancel extends OrderDuration with Parameterless

  case class GoodTillTime(expiration: Instant) extends OrderDuration {
    def kind = GoodTillTime
  }

  object GoodTillTime extends OrderDuration.Kind {
    def apply(expiration: ReadableInstant): GoodTillTime = GoodTillTime(expiration.toInstant)

    def matches(duration: OrderDuration) = duration.isInstanceOf[GoodTillTime]
  }

  case object AtTheOpening extends OrderDuration with Parameterless

  case object AtTheClose extends OrderDuration with Parameterless

}
