package eu.exante.broker.model

import com.typesafe.config.Config
import eu.exante.broker.model.LegalEntityMapping.MappingKey

import scala.collection.JavaConverters._

object LegalEntityMapping {

  private def optString(config: Config, key: String): Option[String] =
    if (config.hasPath(key)) Some(config.getString(key)) else None

  case class MappingKey(clientEntity: String, brokerEntity: String)

  def fromConfig(config: Config): LegalEntityMapping = {
    val accountMapping = config
      .getConfigList("mapping").asScala
      .view
      .map { entry =>
        val key = MappingKey(
          clientEntity = entry.getString("client-entity"),
          brokerEntity = entry.getString("broker-entity"))
        val proxyInfo = LegalEntityInfo(
          user = entry.getString("username"),
          account = entry.getString("account"),
          counterparty = entry.getString("counterparty"),
          counterUser = optString(entry, "counter-username"),
          counterAccount = optString(entry, "counter-account"),
          markupUser = optString(entry, "markup-username"),
          markupAccount = optString(entry, "markup-account"))
        key -> proxyInfo
      }
    new LegalEntityMapping(accountMapping.toMap)
  }

}

case class LegalEntityMapping(accountMapping: Map[LegalEntityMapping.MappingKey, LegalEntityInfo]) {

  def proxyLegalEntity(brokerLegalEntity: String, clientLegalEntity: String): Option[LegalEntityInfo] =
    clientLegalEntity match {
      case `brokerLegalEntity` => None
      case _ =>
        for { entityMapping <- accountMapping.get(MappingKey(clientLegalEntity, brokerLegalEntity)) }
          yield entityMapping
    }

}
