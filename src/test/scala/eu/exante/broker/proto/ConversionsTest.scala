package eu.exante.broker.proto

import java.net.InetAddress
import java.util.UUID

import eu.exante.broker.model.OrderDuration.GoodTillTime
import eu.exante.broker.proto.Conversions._
import eu.exante.broker.{model, proto}
import eu.exante.commons.protobuf.Decimal
import io.protostuff.ByteString
import org.joda.time.{DateTime, Instant}
import org.junit.runner.RunWith
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.GeneratorDrivenPropertyChecks._

import scala.reflect.ClassTag

@RunWith(classOf[JUnitRunner])
class ConversionsTest extends FreeSpec {

  private def testConversion[T, P](values: Gen[T])(implicit toProto: T => P, fromProto: P => T) {
    forAll(values) { value =>
      val converted = toProto(value)
      val convertedBack = fromProto(converted)
      assert(convertedBack === value)
    }
  }

  private def enumGen[T <: Enum[T]: ClassTag]: Gen[T] = {
    val constants = implicitly[ClassTag[T]].runtimeClass.asInstanceOf[Class[T]].getEnumConstants: Seq[T]
    Gen.oneOf(constants)
  }

  private def boolGen: Gen[Boolean] = implicitly[Arbitrary[Boolean]].arbitrary

  "Conversion helper should convert" - {
    "UUID values" in {
      testConversion[UUID, ByteString](Gen.oneOf(Seq(UUID.randomUUID())))
    }

    def decimals = Gen.oneOf[BigDecimal](-1.0, 1.0, 0, -2.1, 2.1, -1000000000.01, 1000000000.01)

    "decimal values" in {
      testConversion[BigDecimal, Decimal](decimals)
    }

    "optional decimal values" in {
      testConversion[Option[BigDecimal], Decimal](Gen.option(decimals))
    }

    "order durations" in {
      val basic = Gen.oneOf(
        model.OrderDuration.Day,
        model.OrderDuration.FillOrKill,
        model.OrderDuration.ImmediateOrCancel,
        model.OrderDuration.AtTheOpening,
        model.OrderDuration.AtTheClose,
        model.OrderDuration.GoodTillCancel)

      val gtt = for {
        timestamp <- Gen.choose(Long.MinValue, Long.MaxValue)
      } yield GoodTillTime(new Instant(timestamp))

      testConversion[model.OrderDuration, proto.OrderDuration](Gen.oneOf(basic, gtt))
    }

    "order duration kinds" in {
      import model.OrderDuration._
      val durations = Gen.oneOf(Day, GoodTillCancel, AtTheOpening, ImmediateOrCancel, FillOrKill, GoodTillTime, AtTheClose)
      testConversion[model.OrderDuration.Kind, proto.OrderDuration.Type](durations)
    }

    "order types" in {
      val basic = Gen.const(model.OrderType.Market)
      val limit = decimals.map(model.OrderType.Limit(_))
      val stop = decimals.map(model.OrderType.Stop(_))
      val stopLimit = for {
        limitPrice <- decimals
        stopPrice <- decimals
      } yield model.OrderType.StopLimit(limitPrice, stopPrice)

      testConversion[model.OrderType, proto.OrderType](Gen.oneOf(
        basic,
        limit,
        stop,
        stopLimit))
    }

    "order type kinds" in {
      import model.OrderType._
      val orderTypes = Gen.oneOf(Market, Limit, Stop, StopLimit)
      testConversion[model.OrderType.Kind, proto.OrderType.Type](orderTypes)
    }

    "sides" in {
      testConversion[model.Side, proto.Side](enumGen[model.Side])
    }

    def errorInfo =
      for {
        group <- enumGen[model.ErrorGroup]
        code <- enumGen[model.ErrorCode]
      } yield model.ErrorInfo(group, code, "Client message", "Internal message")

    "reject reasons" in {
      testConversion[model.ErrorInfo, proto.ErrorInfo](errorInfo)
    }

    "order statuses" in {
      val constants = Gen.oneOf(
        model.OrderStatus.Accepted,
        model.OrderStatus.Created,
        model.OrderStatus.Placing,
        model.OrderStatus.Working,
        model.OrderStatus.Cancelled,
        model.OrderStatus.Pending,
        model.OrderStatus.Filled)
      val statuses = Gen.oneOf(constants, errorInfo.map(model.OrderStatus.Rejected))
      testConversion[model.OrderStatus, proto.OrderStatus](statuses)
    }

    "originator" in {
      val originator = for {
        source <- enumGen[model.OrderSource]
        username <- Gen.alphaStr
        module <- Gen.option(Gen.alphaStr)
        tag <- Gen.option(Gen.alphaStr)
        addr <- Gen.option(Gen.oneOf(
          InetAddress.getByName("192.168.1.1"),
          InetAddress.getByName("8.8.8.8"),
          InetAddress.getByName("2001:cdba::3257:9652")))
      } yield model.Originator(username, source, module = module, tag = tag, address = addr)

      testConversion[model.Originator, proto.Originator](originator)
    }

    "event timestamps" in {
      val timestamps = Gen.oneOf(
        model.EventTimestamp.local(new DateTime),
        model.EventTimestamp.counterparty(new DateTime, new DateTime))
      testConversion[model.EventTimestamp, proto.EventTimestamp](timestamps)
    }

    "schedule behaviors" in {
      implicit val sbFromProto: proto.ScheduleBehavior => model.ScheduleBehavior = scheduleBehaviorFromProto(_, false)
      testConversion[model.ScheduleBehavior, proto.ScheduleBehavior](enumGen[model.ScheduleBehavior])
    }

    "legacy schedule behaviors" in {
      assert(scheduleBehaviorFromProto(proto.ScheduleBehavior.LEGACY, false) === model.ScheduleBehavior.AlwaysPlace)
      assert(scheduleBehaviorFromProto(proto.ScheduleBehavior.LEGACY, true) === model.ScheduleBehavior.Delay)
    }

    "order emulation flags" in {
      val flags = for {
        gtc <- boolGen
        stop <- boolGen
      } yield model.OrderEmulationFlags(goodTillCancel = gtc, stop = stop)
      testConversion[model.OrderEmulationFlags, proto.OrderEmulationFlags](flags)
    }
  }

}
